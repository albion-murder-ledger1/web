import { PlayerInfo } from 'src/models/player_info';
import { GenericItem } from 'src/components/Item';
import { Typography } from 'antd'

const { Link } = Typography;


type PlayerHeaderProps = {
    player: PlayerInfo
};

export const PlayerHeader = ({ player }: PlayerHeaderProps) => {
    const twitch_link = player.twitch_name ? (
        <div><Link href={`https://twitch.tv/${player.twitch_name}`} target="_blank">twitch.tv/{player.twitch_name}</Link></div>
    ) : null;
    const slayer_rank = player.elo_1v1_slayer ? (<div>1v1 Slayer Rank: <strong>{player.rank_1v1_slayer}</strong> (MMR {player.elo_1v1_slayer})</div>) : null;
    const stalker_rank = player.elo_1v1_stalker ? (<div>1v1 Stalker Rank: <strong>{player.rank_1v1_stalker}</strong> (MMR {player.elo_1v1_stalker})</div>) : null;
    const rank_2v2 = player.rating_2v2 ? (<div>2v2 Rank: <strong>{player.rank_2v2}</strong> (MMR {player.rating_2v2})</div>) : null;
    const img = player.fav_weapon ? (<GenericItem size={40} item={player.fav_weapon ?? ''} />) : null;

    return (
        <>
            <h1 style={{ fontSize: '35px', fontWeight: 'bolder' }}>{player.name} {img}</h1>
            {twitch_link}
            {slayer_rank}
            {stalker_rank}
            {rank_2v2}
        </>
    );
}
