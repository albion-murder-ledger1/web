import { Item as ItemModel } from 'src/models/event';
import { weapons } from 'src/models/weapons';
import { apiRoot } from 'src/api';

type ItemProps = { item: ItemModel, size?: number };

export const Item = ({ item, size = 60 }: ItemProps) => {
    if (item.id === '') {
        return (
            <div style={{ display: 'inline-block', padding: '3px', marginLeft: '-3px' }}>
                <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', width: `${size - 6}px`, height: `${size - 6}px`, borderRadius: '7px' }}></div>
            </div>
        );
    }

    const title = `${item.en_name} (${item.tier}.${item.enchant})`;

    return (
        <img
            title={title}
            src={`${apiRoot}/items/${item.id}.webp?quality=${item.quality}`}
            width={size}
            height={size}
            style={{ margin: "0 -2px" }}
            alt=""
        />
    );
};

type GenericItemProps = {
    item: keyof typeof weapons,
    size?: number,
    tooltip?: string,
};

export const GenericItem = ({ item, size = 60, tooltip = '' }: GenericItemProps) => {
    if (!item) {
        return (
            <div style={{ display: 'inline-block', padding: '3px' }}>
                <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', width: `${size - 6}px`, height: `${size - 6}px`, borderRadius: '7px' }}></div>
            </div>
        );
    }

    const id = `T8_${item}`;
    const title = tooltip ? tooltip : weapons[item];

    return (
        <img title={title} src={`${apiRoot}/items/${id}.webp`} width={size} height={size} alt="" />
    );
};
