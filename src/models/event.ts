export interface Event {
    time: number;
    id: number;
    battle_id: number;
    killer: Killer;
    victim: Player;
    total_kill_fame: number;
    participant_count: number;
    party_size: number;
    tags: { [key: string]: boolean };
}

export interface Player {
    name: string;
    item_power: number;
    guild_name: string;
    alliance_name: string;
    loadout: Loadout;
    vod: string;
    elo_change: number;
    rank_1v1: number;
}

export interface Killer extends Player {
    is_primary: boolean;
    kill_fame: number;
    damage_done: number;
    healing_done: number;
}

export interface Loadout {
    main_hand: Item;
    off_hand: Item;
    head: Item;
    body: Item;
    shoe: Item;
    bag: Item;
    cape: Item;
    mount: Item;
    food: Item;
    potion: Item;
}

export interface Item {
    id: string;
    type: string;
    tier: number;
    enchant: number;
    quality: number;
    en_name: string;
}
