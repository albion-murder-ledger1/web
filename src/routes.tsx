import { PlayerPage } from 'src/pages/PlayerPage';
import { BuildStatsPage } from 'src/pages/BuildStatsPage';
import { LeaderboardPage } from 'src/pages/LeaderboardPage';
import { HomePage } from 'src/pages/HomePage';
import { VodsPage } from 'src/pages/VodsPage';
import { AboutPage } from 'src/pages/AboutPage';
import { SeasonsPage } from 'src/pages/SeasonsPage';
import { WeaponMatrixPage } from 'src/pages/WeaponMatrixPage';

export const routes = [
    {
        path: "/players/:name/:tab",
        main: () => <PlayerPage />
    },
    {
        path: "/leaderboards/:tab",
        main: () => <LeaderboardPage />
    },
    {
        path: "/builds/:dataset",
        main: () => <BuildStatsPage />
    },
    {
        path: "/weapon-matrix",
        main: () => <WeaponMatrixPage />
    },
    {
        path: "/vods",
        main: () => <VodsPage />
    },
    {
        path: "/about",
        main: () => <AboutPage />
    },
    {
        path: "/seasons",
        main: () => <SeasonsPage />
    },
    {
        path: "/",
        main: () => <HomePage />
    },
    {
        path: "*",
        main: () => <span>Page not found</span>
    }
];
