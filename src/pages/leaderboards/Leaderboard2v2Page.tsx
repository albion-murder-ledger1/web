import { useEffect, useState, useMemo } from 'react';
import { DateTime } from "luxon";
import { Select, Table, Spin, Space, Input, Alert } from 'antd';
import { useHistory, useLocation, Link } from 'react-router-dom';

import { useDebounceEffect } from 'src/util/useDebounceEffect';
import { getRelativeTime } from 'src/util/relativeTime';
import { Leaderboard, Player, Season } from 'src/models/leaderboard';
import { api } from 'src/api';
import { GenericItem } from 'src/components/Item';
import { TwitchLink } from 'src/components/TwitchLink';
import { WeaponsSelect } from 'src/components/WeaponsSelect';

const { Option } = Select;

type Leaderboard2v2PageProps = {
    seasons: Array<Season>,
    onSyncDelayChange: (delay: number) => void,
};

export const Leaderboard2v2Page = ({ seasons, onSyncDelayChange }: Leaderboard2v2PageProps) => {
    const history = useHistory();

    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [leaderboard, setLeaderboard] = useState<Leaderboard | null>(null);
    const [q, setQ] = useState<string>(queryParams.get('q') ?? '');
    const [debouncedQ, setDebouncedQ] = useState<string>(q);
    const [weapon, setWeapon] = useState<string>(queryParams.get('weapon') ?? '');
    const [selectedSeason, setSelectedSeason] = useState<string>(queryParams.get('season') ?? '');

    const [error, setError] = useState<string | null>(null);

    useEffect(() => {
        const params = new URLSearchParams()

        if (debouncedQ !== '') {
            params.append("q", debouncedQ);
        } else {
            params.delete("q");
        }

        if (weapon !== '') {
            params.append("weapon", weapon);
        } else {
            params.delete("weapon");
        }

        if (selectedSeason !== '') {
            params.append("season", selectedSeason);
        } else {
            params.delete("season");
        }

        history.replace({ search: params.toString() })

        params.set('take', '100');
        api<Leaderboard>(`/leaderboards/2v2?${params.toString()}`)
            .then(lb => {
                setLeaderboard(lb);
                onSyncDelayChange(lb.sync_delay);
            })
            .catch((error) => setError(String(error)));
    }, [debouncedQ, weapon, history, selectedSeason, onSyncDelayChange]);

    useDebounceEffect(
        () => setDebouncedQ(q),
        500,
        [q],
    );

    const filters = useMemo(() => (
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'end' }}>
            <Space size="large" direction="horizontal" wrap>
                <div>
                    <div>Search</div>
                    <Input.Search
                        value={q}
                        allowClear
                        placeholder="Search for player"
                        onChange={e => setQ(e.target.value)} style={{ width: 220 }}
                    />
                </div>
                <WeaponsSelect value={weapon} setValue={setWeapon} />
                <div>
                    <div>Season</div>
                    <Select
                        value={selectedSeason}
                        defaultValue=""
                        style={{ width: 230 }}
                        onSelect={v => setSelectedSeason(v)}
                    >
                        {seasons.map(season => {
                            const start = DateTime.fromSeconds(season.start_date).toFormat('LLL d');
                            const end = DateTime.fromSeconds(season.end_date).toFormat('LLL d');
                            return (
                                <Option key={season.id} value={season.is_current ? '' : season.id}>
                                    {season.name}{season.is_current ? ' (current)' : ` ${start} to ${end}`}
                                </Option>
                            );
                        })}
                    </Select>
                </div>
            </Space>
        </div>
    ), [q, weapon, selectedSeason, seasons]);

    if (error) {
        return (
            <div style={{ marginTop: '80px' }}>
                <Alert
                    type="error"
                    message={
                        'There was an error loading data'
                    }
                    description={error}
                    banner
                />
            </div>
        );
    }

    if (!leaderboard) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                <Spin size="large" />
            </div>
        );
    }

    return (
        <Space direction="vertical" style={{ width: "100%" }}>
            {filters}
            <Table
                size="small"
                pagination={false}
                dataSource={leaderboard.data}
                rowKey={(row: Player) => row.name}
            >
                <Table.Column
                    title="Rank"
                    dataIndex="rank"
                    key="rank"
                    align="center"
                />
                <Table.Column
                    title="Player"
                    dataIndex="name"
                    key="name"
                    render={(name: string, player: Player) => (
                        <Space>
                            <GenericItem
                                size={35}
                                item={player.favorite_weapon_item}
                            />
                            <Link to={`/players/${name}/2v2s`}>
                                {name}
                            </Link>
                            {player.twitch_username ? (
                                <TwitchLink
                                    link={`https://twitch.tv/${player.twitch_username}`}
                                />
                            ) : null}
                        </Space>
                    )}
                />
                <Table.Column
                    title="Rating"
                    dataIndex="rating"
                    key="rating"
                />
                {selectedSeason === "" ? (
                    <Table.Column
                        title="Last Update"
                        dataIndex="last_update"
                        key="last_update"
                        render={(timestamp) => {
                            if (!timestamp) {
                                return '';
                            }
                            return getRelativeTime(new Date(timestamp * 1000));
                        }}
                    />
                ) : null}
            </Table>
        </Space>
    );
};


