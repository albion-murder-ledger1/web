FROM node:16-alpine as build

USER root
RUN mkdir -p /app
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm ci --silent
COPY src ./src
COPY public ./public
COPY tsconfig.json tsconfig.node.json vite.config.ts index.html .env ./
RUN npm run build

FROM nginx:stable-alpine

COPY --from=build /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
