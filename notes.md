## Big List (MVP)
One big list of all events that has support for many kinds of
filtering, sorting, and searching.

### Entry Points
 - `/api/events` All events
 - `/api/players/:player-name/events` All events for player
 - `/api/guilds/:guild-name/events` All events for guild

### Response
```
{
    events: [Event],
    skip: Int,
    take: Int,
    totalCount: Int
}
```

### Filters
- Event Type
    Can be kills, deaths, assists, all
    Only for use with player endpoint

    Default: kills, deaths

- Search
    Allows search for player names, guild names, and
    alliance tags

    Default: empty

- Date Range
    Filter only kills within a date range

    Default: all time

- Participant Count
    Filter based on number of participants. Maybe include
    1v1, 2v2, and any

    Default: any

- Item Power
    Filter based on the killers average item power

    Default: any value

- Fame
    Filter based on total kill fame

    Default: any value
